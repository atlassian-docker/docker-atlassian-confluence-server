**Note**: This Docker image has been published as both `atlassian/confluence` and `atlassian/confluence-server` up until February 15, 2024.
Both names refer to the same image. However, post-February 15, 2024, the `atlassian/confluence-server` version ceased receiving updates, including both existing and new tags.
If you have been using `atlassian/confluence-server`, switch to the `atlassian/confluence` image to ensure access to the latest updates and new tags.

---

![Atlassian Confluence Server](https://wac-cdn.atlassian.com/dam/jcr:5d1374c2-276f-4bca-9ce4-813aba614b7a/confluence-icon-gradient-blue.svg?cdnVersion=696)

Confluence Server is where you create, organise and discuss work with your
team. Capture the knowledge that's too often lost in email inboxes and shared
network drives in Confluence - where it's easy to find, use, and update. Give
every team, project, or department its own space to create the things they need,
whether it's meeting notes, product requirements, file lists, or project plans,
you can get more done in Confluence.

Learn more about Confluence Server: <https://www.atlassian.com/software/confluence>

You can find the repository with the Dockerfile at <https://bitbucket.org/atlassian-docker/docker-atlassian-confluence-server>

# Overview

This Docker container makes it easy to get an instance of Confluence up and
running.

**Use docker version >= 20.10.10**

# Quick Start

For the directory in the environmental variable `CONFLUENCE_HOME` that is used
to store Confluence data (amongst other things) we recommend mounting a host
directory as a [data volume][1]:

Additionally, if running Confluence in Data Center mode it is required that a
shared filesystem is mounted. The mountpoint (inside the container) can be
configured with `CONFLUENCE_SHARED_HOME`.

Start Atlassian Confluence Server:

    docker run -v /data/your-confluence-home:/var/atlassian/application-data/confluence --name="confluence" -d -p 8090:8090 -p 8091:8091 atlassian/confluence


**Success**. Confluence is now available on <http://localhost:8090>*

Please ensure your container has the necessary resources allocated to it.  We
recommend 2GiB of memory allocated to accommodate the application server.  See
[Supported Platforms][3] for further information.

_* Note: If you are using `docker-machine` on Mac OS X, please use `open http://$(docker-machine ip default):8090` instead._

# Advanced Usage
For advanced usage, e.g. configuration, troubleshooting, supportability, etc.,
please check the [**Full Documentation**](https://atlassian.github.io/data-center-helm-charts/containers/CONFLUENCE/).


[1]: https://docs.docker.com/userguide/dockervolumes/#mount-a-host-directory-as-a-data-volume
[3]: https://confluence.atlassian.com/display/DOC/Supported+platforms