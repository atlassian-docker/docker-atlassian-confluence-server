
from pathlib import Path
import os
import jinja2 as j2

TEMPLATE_FILE = 'bitbucket-pipelines.yml.j2'
REPOS = ['atlassian/confluence']

images = {
    'Confluence': {
        11: {
            'start_version': '8.4',
            'end_version': '9.0',
            'default_release': False,
            'default_eap': False,
            'base_image': 'eclipse-temurin:11-noble',
            'tag_suffixes': ['jdk11','ubuntu-jdk11'],
            'dockerfile': 'Dockerfile',
            'docker_repos': REPOS,
        },
        17: {
            'start_version': '8.4',
            'end_version': '10.0',
            'default_release': True,
            'default_eap': True,
            'base_image': 'eclipse-temurin:17-noble',
            'tag_suffixes': ['jdk17','ubuntu-jdk17'],
            'dockerfile': 'Dockerfile',
            'docker_repos': REPOS,
        },
        "17-ubi": {
            'start_version': '8.5.6',
            'end_version': '10.0',
            'default_release': False,
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-17',
            'tag_suffixes': ['ubi9','ubi9-jdk17'],
            'dockerfile': 'Dockerfile.ubi',
            'docker_repos': REPOS,
            'snyk_threshold': 'critical'
        },
        21: {
            'start_version': '9.0',
            'end_version': '9.1',
            'default_release': False,
            'default_eap': False,
            'base_image': 'eclipse-temurin:21-noble',
            'tag_suffixes': ['jdk21','ubuntu-jdk21'],
            'dockerfile': 'Dockerfile',
            'docker_repos': REPOS,
            'batches': 1,
        },
        "21-default": {
            'start_version': '9.1',
            'default_release': True,
            'default_eap': True,
            'base_image': 'eclipse-temurin:21-noble',
            'tag_suffixes': ['jdk21','ubuntu-jdk21'],
            'dockerfile': 'Dockerfile',
            'docker_repos': REPOS,
            'batches': 2,
        },
        "21-ubi": {
            'start_version': '9.0',
            'default_release': False,
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-21',
            'tag_suffixes': ['ubi9','ubi9-jdk21'],
            'dockerfile': 'Dockerfile.ubi',
            'docker_repos': REPOS,
            'snyk_threshold': 'critical',
            'batches': 2,
        }
    },
}


def main():
    jenv = j2.Environment(
        loader=j2.FileSystemLoader('.'),
        lstrip_blocks=True,
        trim_blocks=True)
    template = jenv.get_template(TEMPLATE_FILE)
    generated_output = template.render(images=images, batches=8)

    print(generated_output)

if __name__ == '__main__':
    main()
